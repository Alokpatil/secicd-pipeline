// import files
var add = require("./calculator/add");
var subtract = require("./calculator/subtract");
var multiply = require("./calculator/multiply");
var divide = require("./calculator/divide");

window.onload = function () {
  document.getElementById("calculate").addEventListener("click", calculate);
};

function calculate() {
  var firstNum = parseInt(document.getElementById("firstnumber").value);
  // var secondNum = parseInt(document.getElementById("secondnumber").value);
  var operator = document.getElementById("operator").value;
  var resultElm = document.getElementById("result");
  var result;

  try {
    switch (operator) {
      case "add":
        result = add(firstNum);
        break;
      case "subtract":
        result = subtract(firstNum);
        break;
      case "multiply":
        result = multiply(firstNum);
        break;
      case "divide":
        result = divide(firstNum);
        break;
      default:
        alert("Error!");
        break;
    }
    resultElm.textContent = "Result: " + result;
    resultElm.style.color = "black";
  } catch (error) {
    resultElm.textContent = "Error: " + error.message;
    resultElm.style.color = "red";
  }
}
